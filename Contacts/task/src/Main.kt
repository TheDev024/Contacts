package contacts

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.io.File
import java.lang.reflect.ParameterizedType
import java.time.Instant
import java.util.*

val scanner = Scanner(System.`in`)

val PHONE_NUMBER_REGEX =
    "\\+?(\\([\\da-zA-Z]+\\)|[\\da-zA-Z]+[\\s-]\\([\\da-zA-Z]{2,}\\)|[\\da-zA-Z]+)([\\s-][\\da-zA-Z]{2,})*".toRegex()
val BIRTHDATE_REGEX = "\\d{4}(-\\d{1,2}){2}".toRegex()

open class Contact(phoneNumber: String = "[no data]", private val createTime: String, var lastEditTime: String) {
    var phoneNumber: String = phoneNumber // "[no data]"
        set(value) {
            field = if (PHONE_NUMBER_REGEX.matches(value) || value == "[no data]") value
            else {
                println("Wrong number format!")
                "[no data]"
            }
        }
    open val name = ""
    open val searchQuery: String
        get() = "$phoneNumber\n$createTime\n$lastEditTime"

    init {
        this.phoneNumber = phoneNumber
    }

    override fun toString(): String = "Number: $phoneNumber\nTime created: $createTime\nTime last edit: $lastEditTime\n"
}

class Person(
    var firstName: String,
    var lastName: String,
    birthDate: String = "[no data]",
    gender: String = "[no data]",
    phoneNumber: String = "[no data]",
    val createTime: String,
    lastEditTime: String
) : Contact(
    phoneNumber, createTime, lastEditTime
) {
    var birthDate = birthDate
        set(value) {
            field = if (BIRTHDATE_REGEX.matches(value)) value
            else {
                println("Bad birth date!")
                "[no data]"
            }
        }
    var gender = gender
        set(value) {
            field = if (value in listOf("M", "F")) value
            else {
                println("Bad gender!")
                "[no data]"
            }
        }
    override val name: String
        get() = "$firstName $lastName"
    override val searchQuery: String
        get() = "$firstName\n$lastName\n$birthDate\n$gender\n" + super.searchQuery

    override fun toString(): String =
        "Name: $firstName\nSurname: $lastName\nBirth date: $birthDate\nGender: $gender\n" + super.toString()
}

class Organization(
    override var name: String,
    var address: String,
    phoneNumber: String = "[no data]",
    val createTime: String,
    lastEditTime: String
) : Contact(phoneNumber, createTime, lastEditTime) {
    override val searchQuery: String
        get() = "$name\n$address\n" + super.searchQuery

    override fun toString(): String = "Organization name: $name\nAddress: $address\n" + super.toString()
}

data class Record(val type: String, val json: String)

class PhoneBook {
    private val recordFile: File
    private val contactList = mutableListOf<Contact>()
    private val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    private val recordType: ParameterizedType = Types.newParameterizedType(List::class.java, Record::class.java)
    private val recordAdapter: JsonAdapter<List<Record>> = moshi.adapter(recordType)
    private val personAdapter: JsonAdapter<Person> = moshi.adapter(Person::class.java)
    private val organizationAdapter: JsonAdapter<Organization> = moshi.adapter(Organization::class.java)

    init {
        val recordPath = getInput("open ")
        this.recordFile = File(recordPath)
        if (!recordFile.exists()) recordFile.createNewFile()
        val data = recordFile.readText()
        val records = if (data.isNotBlank()) recordAdapter.fromJson(data) else emptyList()
        records!!.forEach { record ->
            when (record.type) {
                "person" -> {
                    val person = personAdapter.fromJson(record.json)
                    contactList.add(person!!)
                }

                "organization" -> {
                    val organization = organizationAdapter.fromJson(record.json)
                    contactList.add(organization!!)
                }
            }
        }
        println()
    }

    fun start() {
        while (true) {
            when (getOptions(
                "[menu] Enter action (add, list, search, count, exit): ", "add|list|search|count|exit".toRegex()
            )) {
                "add" -> addContact()

                "list" -> listContacts()

                "search" -> searchContacts()

                "count" -> println("The Phone Book has ${contactList.size} record${if (contactList.size == 1) "" else "s"}.\n")

                "exit" -> break
            }
        }
    }

    private fun searchContacts() {
        val query = getInput("Enter search query: ")
        val result = contactList.filter { it.searchQuery.lowercase().contains(query.lowercase()) }
        println("Found ${result.size} result${if (result.size == 1) "" else "s"}:")
        result.withIndex().forEach { contact -> println("${contact.index + 1}. ${contact.value.name}") }
        while (true) {
            when (val input =
                getOptions("[search] Enter action ([number], back, again): ", "\\d+|back|again".toRegex())) {
                "back" -> {
                    println()
                    break
                }

                "again" -> continue

                else -> {
                    val index = input.toInt() - 1
                    getRecord(index)
                    break
                }
            }
        }

    }

    private fun getRecord(index: Int) {
        println("${contactList[index]}\n")
        while (true) {
            when (getOptions("[record] Enter action (edit, delete, menu): ", "edit|delete|menu".toRegex())) {
                "edit" -> editRecord(index)

                "delete" -> {
                    contactList.removeAt(index)
                    println("The record deleted.\n")
                    saveRecords()
                }

                else -> {
                    println()
                    break
                }
            }
        }

    }

    private fun editRecord(index: Int) {
        val currentTime = Instant.now().toString().substring(0..15)
        val contact = contactList[index]
        when (contact) {
            is Person -> {
                when (getOptions(
                    "Select a field (name, surname, birth, gender, number): ",
                    "name|surname|birth|gender|number".toRegex()
                )) {
                    "name" -> contact.firstName = getInput("Enter name: ")

                    "surname" -> contact.lastName = getInput("Enter surname: ")

                    "birth" -> contact.birthDate = getInput("Enter birth date: ")

                    "gender" -> contact.gender = getInput("Enter gender (M, F): ")

                    "number" -> contact.phoneNumber = getInput("Enter number: ")
                }
            }

            is Organization -> {
                when (getOptions("Select a field (name, address, number): ", "name|address|number".toRegex())) {
                    "name" -> contact.name = getInput("Enter name: ")

                    "address" -> contact.address = getInput("Enter address: ")

                    "number" -> contact.phoneNumber = getInput("Enter number: ")
                }
            }
        }
        contact.lastEditTime = currentTime
        saveRecords()
        println("Saved\n$contact\n")
    }

    private fun listContacts() {
        printList()
        when (val input = getOptions("[list] Enter action ([number], back): ", "\\d+|back".toRegex())) {
            "back" -> println()

            else -> {
                val index = input.toInt() - 1
                getRecord(index)
            }
        }
    }

    private fun printList() {
        contactList.withIndex().forEach { contact -> println("${contact.index + 1}. ${contact.value.name}") }
        println()
    }

    private fun addContact() {
        val currentTime = Instant.now().toString().substring(0..15)
        when (getOptions("Enter the type (person, organization): ", "person|organization".toRegex())) {
            "person" -> {
                val contact = Person(
                    getInput("Enter the name: "),
                    getInput("Enter the surname: "),
                    createTime = currentTime,
                    lastEditTime = currentTime
                )
                contact.birthDate = getInput("Enter the birthdate: ")
                contact.gender = getInput("Enter the gender(M, F): ")
                contact.phoneNumber = getInput("Enter the number: ")
                contactList.add(contact)
            }

            "organization" -> {
                val contact = Organization(
                    getInput("Enter the organization name: "),
                    getInput("Enter the address: "),
                    getInput("Enter the number: "),
                    createTime = currentTime,
                    lastEditTime = currentTime
                )
                contactList.add(contact)
            }
        }
        println("The record added.\n")
        saveRecords()
    }

    private fun saveRecords() {
        val records = contactList.map { contact ->
            when (contact) {
                is Person -> Record(
                    "person", personAdapter.toJson(contact)
                )

                is Organization -> Record(
                    "organization", organizationAdapter.toJson(contact)
                )

                else -> Record(
                    "null", "{}"
                )
            }
        }
        val recordsJson = recordAdapter.toJson(records)
        recordFile.writeText(recordsJson)
    }
}

fun getOptions(message: String, options: Regex): String {
    val option = getInput(message)
    return if (options.matches(option)) option else getOptions(message, options)
}

fun getInput(message: String): String {
    print(message)
    return scanner.nextLine()
}

fun main() {
    PhoneBook().start()
}
